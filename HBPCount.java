package hw1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.hadoop.io.*;

/**
 * 
 * @M16-522 Medvedev V.E.
 */
public class HBPICount extends Configured implements Tool {
	/**
	 * @параметры строки
	 *            папка входящих и папка исходящих в hdfs.
	 */
	public static void main(String args[]) throws Exception {
		// запуск джобы с toolrunner
		int res = ToolRunner.run(new Configuration(), new HBPICount(), args);
		System.exit(res);
	}

	/**
	 * запуск работы с настройками
	 */
	public int run(String[] args) throws Exception {

		// создание работы
		Configuration conf = this.getConf();
		Job job = Job.getInstance(conf, "HBPICount");
		job.setJarByClass(HBPICount.class);

		// setting классов для мап-редьюса
		job.setMapperClass(Map.class);
		job.setPartitionerClass(CustomPartitioner.class);
		job.setReducerClass(Reduce.class);

		// добавление файлов в дистрибутивный кэш
		job.addCacheFile(new URI("/hw1/city.en.txt"));
		job.addCacheFile(new URI("/hw1/region.en.txt"));

		// входные/выходные key/value
		job.setOutputKeyClass(Text.class);
		job.setOutputValueClass(IntWritable.class);
		job.setMapOutputKeyClass(IntOpWritableComparable.class);
		job.setMapOutputValueClass(IntWritable.class);

		// установка выходного формата файлов
		job.setOutputFormatClass(CSVOutputFormat.class);

		// спец вход исход части
		FileInputFormat.setInputPaths(job, new Path(args[0]));
		FileOutputFormat.setOutputPath(job, new Path(args[1]));

		// запуск задания
		job.waitForCompletion(true);
		if (job.isSuccessful()) {
			return 0;
		} else
			return 1;
	}

	/**
	 * Класс мапинга входящих ключей/значений в преможуточные ключи/значения
	 */
	public static class Map extends Mapper<Object, Text, IntOpWritableComparable, IntWritable> {
		private final static IntWritable one = new IntWritable(1);

		// столбцы в исх файле
		private static final int MIN_BID_PRICE = 250;
		private static final int BID_INDEX = 19;
		private static final int CITY_INDEX = 7;
		private static final int MAX_INDEX = 24;
		private static final int OP_INDEX = 4;

		/**
		 *  Мапинг входящих ключей/значений в преможуточные ключи/значения с большим прайсом
		 * 
		 */
		public void map(Object key, Text value, Context context) throws IOException, InterruptedException {
			String[] columns = value.toString().split("\\t");
			if (columns.length < MAX_INDEX) {
				return;
			}
			try {
				int cityId = Integer.parseInt(columns[CITY_INDEX]);
				int bidPrice = Integer.parseInt(columns[BID_INDEX]);
				String operatingSystem = columns[OP_INDEX];
				IntOpWritableComparable keyOut = new IntOpWritableComparable(cityId, operatingSystem);
				if (bidPrice > MIN_BID_PRICE) {
					context.write(keyOut, one);
				}
			} catch (NumberFormatException e) {
			}
		}
	}

	/**
	 * кастомный партишионар ОС
	 */
	public static class CustomPartitioner extends Partitioner<IntOpWritableComparable, IntWritable> {

		/**
		 * Partitions the key space by operating system to choose a reducer. Needs
		 * IntOpWritableComparableType for key to use information of operating system.
		 * Bad algorithm. Made because of task in making custom partitioner.
		 */
		@Override
		public int getPartition(IntOpWritableComparable key, IntWritable value, int numReduceTasks) {
			int osType = 0;
			if (numReduceTasks == 0)
				return 0;
			Pattern pattern = Pattern.compile(".*\\(.+\\).*");
			Matcher matcher = pattern.matcher(key.getOS());
			if (matcher.find()) {
				Pattern pattern1 = Pattern.compile("Windows", Pattern.CASE_INSENSITIVE);
				Matcher matcher1 = pattern1.matcher(key.getOS());
				if (matcher1.find()) {
					osType = 1;
				} else {
					Pattern pattern2 = Pattern.compile("Macintosh", Pattern.CASE_INSENSITIVE);
					Matcher matcher2 = pattern2.matcher(key.getOS());
					if (matcher2.find()) {
						osType = 2;
					} else {
						Pattern pattern3 = Pattern.compile("ip(hone|ad)", Pattern.CASE_INSENSITIVE);
						Matcher matcher3 = pattern3.matcher(key.getOS());
						if (matcher3.find()) {
							osType = 3;
						} else {
							Pattern pattern4 = Pattern.compile("(Linux.*Android)|(Android.*Linux))",
									Pattern.CASE_INSENSITIVE);
							Matcher matcher4 = pattern4.matcher(key.getOS());
							if (matcher4.find()) {
								osType = 4;
							} else {
								Pattern pattern5 = Pattern.compile("Linux", Pattern.CASE_INSENSITIVE);
								Matcher matcher5 = pattern5.matcher(key.getOS());
								if (matcher5.find()) {
									osType = 5;
								} else {
									osType = 6;
								}
							}
						}
					}
				}
			} else {
				return (key.hashCode() & Integer.MAX_VALUE) % numReduceTasks;
			}
			return osType % numReduceTasks;
		}
	}

	/**
	 * Клас для мапинга промежуточных ключей/значений в финальные
	 */
	public static class Reduce extends Reducer<IntOpWritableComparable, IntWritable, Text, IntWritable> {

		HashMap<Integer, String> cityList = new HashMap<Integer, String>();

		/**
		 * Чтение мапинговых файлов из  disributed cache и помещение hashmap перед стартом редьюсера
		 */
		@Override
		protected void setup(Context context) throws IOException, InterruptedException {

			// использование кэша
			URI[] cacheFiles = context.getCacheFiles();
			if (cacheFiles != null && cacheFiles.length > 0) {
				BufferedReader reader = null;
				BufferedReader reader1 = null;
				try {
					FileSystem fs = FileSystem.get(context.getConfiguration());
					Path path = new Path(cacheFiles[0].toString());
					reader = new BufferedReader(new InputStreamReader(fs.open(path)));
					String line;
					while ((line = reader.readLine()) != null) {
						String[] columns = line.toString().split("\\s+");
						cityList.put(Integer.parseInt(columns[0]), columns[1]);
					}
					Path path1 = new Path(cacheFiles[1].toString());
					reader1 = new BufferedReader(new InputStreamReader(fs.open(path1)));
					while ((line = reader1.readLine()) != null) {
						String[] columns = line.toString().split("\\s+");
						cityList.put(Integer.parseInt(columns[0]), columns[1]);
					}
				} catch (IOException e) {
					// Игнорирование неверных строкк
				} catch (NumberFormatException e) {
				} finally {
					try {
						reader.close();
						reader1.close();
					} catch (IOException e) {
					}
				}
			}
		}

		/**
		 * Вычисление колличества равных ключей - вычисление суммы самой высокой цены по городам
		 */
		@Override
		public void reduce(IntOpWritableComparable key, Iterable<IntWritable> counts, Context context)
				throws IOException, InterruptedException {
			int sum = 0;
			Text outKey;
			for (IntWritable count : counts) {
				sum += count.get();
			}
			if (!cityList.isEmpty()) {
				String cityName = cityList.get(key.getId());
				if (cityName == null) {
					outKey = new Text("UNKNOWN");
				} else {
					outKey = new Text(cityName);
				}
				context.write(outKey, new IntWritable(sum));
			}
			System.out.print("Cached file is not found\n\r");
		}
	}

}
