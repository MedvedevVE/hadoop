package hw1;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Класс для тестирования IntOpWritableComparable type.
 * 
 * М16-522 Медведев ВЭ
 */
public class IntOpWritableComparableTest {

	/**
	 * Тестирование конструктора с параметрами
	 */
	@Test
	public void constructorTest() {
		IntOpWritableComparable testOb = new IntOpWritableComparable(1, "Mozilla/5.0"
				+ " (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.15" + "00.71 Safari/537.36");
		assertEquals(testOb.getId(), 1);
		assertEquals(testOb.getOS(), "Mozilla/5.0"
				+ " (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/28.0.15" + "00.71 Safari/537.36");
	}

	/**
	 * Тест дефолтного конструктора
	 */
	@Test
	public void constructor2Test() {
		IntOpWritableComparable testOb = new IntOpWritableComparable();
		assertEquals(testOb.getId(), 0);
		assertEquals(testOb.getOS(), "");
	}

	/**
	 * Тестирование хэша
	 */
	@Test
	public void hashCodeTest() {
		IntOpWritableComparable t1 = new IntOpWritableComparable(1, "Windows NT 6.1)");
		IntOpWritableComparable t2 = new IntOpWritableComparable(1, "Windows NT 6.1)");
		IntOpWritableComparable t3 = new IntOpWritableComparable(2, "Windows NT 6.1)");
		assertEquals(t1.hashCode() == t2.hashCode(), true);
		assertEquals(t1.hashCode() != t3.hashCode(), true);
	}

	/**
	 * Сравнение
	 */
	@Test
	public void compareTest() {
		IntOpWritableComparable t1 = new IntOpWritableComparable(1, "Windows NT 6.1)");
		IntOpWritableComparable t2 = new IntOpWritableComparable(1, "Windows NT 6.1)");
		IntOpWritableComparable t3 = new IntOpWritableComparable(2, "Windows NT 6.1)");
		assertEquals(t1.compareTo(t2), 0);
		assertEquals(t1.compareTo(t3), -1);
		assertEquals(t3.compareTo(t1), 1);
	}

	/**
	 * тестирование равенства ключей.
	 */
	@Test
	public void equalsTest() {
		IntOpWritableComparable t1 = new IntOpWritableComparable(1, "Windows NT 6.1)");
		IntOpWritableComparable t2 = new IntOpWritableComparable(1, "Windows NT 6.1)");
		IntOpWritableComparable t3 = new IntOpWritableComparable(2, "Windows NT 6.1)");
		assertEquals(t1.equals(t2), true);
		assertEquals(t1.equals(t3), false);
		assertEquals(t1.equals(new Object()), false);
	}

}
